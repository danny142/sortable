import { Grid } from "@material-ui/core";
import React, { FC, forwardRef, useEffect, useState } from "react";
import { ReactSortable } from "react-sortablejs";
import "../SortAble/style.css";

interface ItemType {
  id: number;
  name: string;
  filtered?: boolean;
}

const CustomComponent = forwardRef<HTMLDivElement, any>((props, ref) => {
  return <Grid  container spacing={1} ref={ref}>{props.children}</Grid>;
});

const SortAble: FC = () => {
  const [state, setState] = useState<ItemType[]>([
    { id: 1, filtered: true, name: "red" },
    { id: 2, name: "gray" },
    { id: 3, name: "green" },
    { id: 4, name: "yellow" },
  ]);

  useEffect(() => {
    console.log("state:", state);
  }, [state]);

//   const addImage = () => {
//     let _state: ItemType[] = [...state];
//     const num: number = state.length + 1;
//     _state.push({ id: num, name: `https://picsum.photos/id/${num}/200/200` });
//     setState(_state);
//   };

  return (
    <div style={{padding: 10}}>
      <ReactSortable
        multiDrag // enables mutidrag
        // OR
        // disabled
        group="groupName"
        animation={20}
        delayOnTouchStart={true}
        delay={2}
        swap // enables swap
        
        // style={{ listStyle: "none" }}
        tag={CustomComponent}
        list={state}
        setList={setState}
      >
        {state.map((item) => (
          <Grid xs={item.id % 2 === 0 ? 8 : 4}  item key={item.id}   >
            <div className="item" style={{backgroundColor: item.name}}>
               <p>{item.id}</p>
            </div>
          </Grid>
          //<li className="li-style" key={item.id}>
          //  <img src={item.name} alt={item.name}/>
          //</li>
        ))}
      </ReactSortable>
      {/* <button onClick={addImage}> Add Image</button> */}
    </div>
  );
};

export default SortAble;
